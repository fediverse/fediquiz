export function shuffle(input) {
    let curr = input.length;
    while(curr != 0) {
        let idx = Math.floor(Math.random() * curr);
        curr--;

        [input[curr], input[idx]] = [input[idx], input[curr]];
    }

    return input;
}
