A quiz dedicated to Fediverse's 14th birthday. Available from
[14th.fediverse.party](https://14th.fediverse.party).

Content (images and questions) licensed under [Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International
License](https://creativecommons.org/licenses/by-nc-sa/4.0/) (see LICENSE.CC).
Code licensed under [GNU Affero General Public License
3.0](https://www.gnu.org/licenses/agpl-3.0.en.html) (see LICENCE.AGPL).
